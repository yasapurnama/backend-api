<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        $employees = [];
        foreach (range(0, 9) as $numb) {
            $fullName = $faker->name;
            $nameArr  = explode(' ', $fullName);

            $employee = [
                'nip'        => $faker->unique()->randomNumber(9),
                'full_name'  => $fullName,
                'nick_name'  => $nameArr[0],
                'birth_date' => $faker->date('Y-m-d', '2000-01-01'),
                'address'    => $faker->address,
                'phone'      => $faker->phoneNumber,
                'mobile'     => $faker->phoneNumber,
                'email'      => $faker->email,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            $employees[] = $employee;
        }

        DB::table('employees')->insert($employees);
    }
}
