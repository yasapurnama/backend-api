<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        DB::table('profiles')->insert([
            [
                'id'         => 1,
                'user_id'    => 1,
                'name'       => $faker->name,
                'address'    => $faker->address,
                'phone'      => $faker->phoneNumber,
                'mobile'     => $faker->phoneNumber,
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now(),
            ],
            [
                'id'         => 2,
                'user_id'    => 2,
                'name'       => $faker->name,
                'address'    => $faker->address,
                'phone'      => $faker->phoneNumber,
                'mobile'     => $faker->phoneNumber,
                'created_at' => Carbon::now(),
                'updated_at' =>  Carbon::now(),
            ]
        ]);
    }
}
