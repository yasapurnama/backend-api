# Backend API Test Case with Lumen Framework

This project base of this test case : [Backend Test](docs/Backend-Test.pdf)

## Installation

- Use __git clone__ to clone project
- Copy __.env.example__ file to __.env__
- Edit databse credentials in __.env__ file
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__
- Run __php artisan swagger-lume:generate__
- Open api documentaion `/api/documentation`

## Screenshot

![Backend API documentation](docs/Backend-API-documentation.png)

![Authorization](docs/Authorization.png)

