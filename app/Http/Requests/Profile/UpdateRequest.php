<?php

namespace App\Http\Requests\Profile;

use Anik\Form\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name'         => 'required',
            'address'      => 'required',
            'phone'        => 'required',
            'email'        => 'required|email',
            'password'     => 'required|min:8',
            'new_password' => 'nullable|min:8',
        ];
    }
}
