<?php

namespace App\Http\Requests\Employee;

use Anik\Form\FormRequest;

class SaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'nip'        => 'required',
            'full_name'  => 'required',
            'nick_name'  => 'required',
            'birth_date' => 'required',
            'address'    => 'required',
            'phone'      => 'required',
            'email'      => 'required'
        ];
    }
}
