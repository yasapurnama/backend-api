<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name'     => 'required',
            'address'  => 'required',
            'phone'    => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:8'
        ];
    }
}
