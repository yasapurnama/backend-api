<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profile\UpdateRequest;
use App\Repository\ProfileRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    protected $profileRepository;

    public function __construct(ProfileRepositoryInterface $profileRepository)
    {
        $this->profileRepository = $profileRepository;        
    }

    /**
     * @OA\Get(
     *      path="/api/profile/{id}",
     *      operationId="getProfileUserById",
     *      tags={"Profile"},
     *      summary="Get Profile information",
     *      description="Returns profile data",
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="Profile id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          description="Profile found.",
     *          response=200,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Profile found."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="200"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  @OA\Property(
     *                     property="id", type="integer",
     *                     title="ID", description="ID of the new profile",
     *                     example="1"
     *                  ),
     *                  @OA\Property(
     *                     property="name", type="string",
     *                     title="Name", description="Name of the new user",
     *                     example="Alexis"
     *                  ),
     *                  @OA\Property(
     *                     property="address", type="string",
     *                     title="Address", description="Address of the new user",
     *                     example="5691 Zella Locks Apt. 678, Lake Ambrosefurt, WI 43793-4790"
     *                  ),
     *                  @OA\Property(
     *                     property="phone", type="string",
     *                     title="Phone Number", description="Phone Number of the new user",
     *                     example="(516) 426-1414"
     *                  ),
     *                  @OA\Property(
     *                     property="mobile", type="string",
     *                     title="Mobile Number", description="Mobile Number of the new user",
     *                     example="+1.265.304.1538"
     *                  ),
     *                  @OA\Property(
     *                      property="user",
     *                      ref="#/components/schemas/User"
     *                  ),
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Profile Not found.",
     *          response=404,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Profile Not found."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="404"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="This action is unauthorized.",
     *          response=403,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="This action is unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="403"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     * )
     */

    public function show($id)
    {
        $profile = $this->profileRepository->findProfileUser($id);

        $this->authorize('view', $profile);

        if ($profile) {

            $result = [
                'message'    => 'Profile found.',
                'statuscode' => 200,
                'data'       => $profile
            ];
        } else {

            $result = [
                'message'    => 'Profile Not found.',
                'statuscode' => 404,
                'data'       => $profile
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

    /**
     * @OA\Put(
     *      path="/api/profile/{id}",
     *      operationId="updateProfileUser",
     *      tags={"Profile"},
     *      summary="Update existing profile user",
     *      description="Returns updated profile status",
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="Profile id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          description="List of profile user object",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  required={
     *                      "name","address","phone","email","password"
     *                  },
     *                  @OA\Property(
     *                      property="name", type="string",
     *                      title="Name", description="Name of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="address", type="string",
     *                      title="Address", description="Address of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="phone", type="string",
     *                      title="Phone Number", description="Phone Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="mobile", type="string",
     *                      title="Mobile Number", description="Mobile Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="email", type="string",
     *                      title="Email", description="Email of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="Password", description="Password of the new user"
     *                  ),
     *                  @OA\Property(
     *                      property="new_password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="New Password", description="New User password"
     *                  ),
     *              )
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  required={
     *                      "name","address","phone","email","password"
     *                  },
     *                  @OA\Property(
     *                      property="name", type="string",
     *                      title="Name", description="Name of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="address", type="string",
     *                      title="Address", description="Address of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="phone", type="string",
     *                      title="Phone Number", description="Phone Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="mobile", type="string",
     *                      title="Mobile Number", description="Mobile Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="email", type="string",
     *                      title="Email", description="Email of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="Password", description="Password of the new user"
     *                  ),
     *                  @OA\Property(
     *                      property="new_password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="New Password", description="New User password"
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Update Profile success.",
     *          response=201,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Update Profile success."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="201"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Update Profile failed.",
     *          response=422,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Update Profile failed."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="422"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Password doesn't match.",
     *          response=403,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Password doesn't match."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="403"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     * )
     */

    public function update(UpdateRequest $request, $id)
    {
        $attributes = $request->only([
            'name', 'address', 'phone', 'mobile',
            'email', 'password', 'new_password'
        ]);

        $attributes['id'] = $id;
        $profile          = $this->profileRepository->findProfileUser($id);

        $this->authorize('update', $profile);

        if (Hash::check($attributes['password'], @$profile->user->password)) {

            if ($this->profileRepository
                    ->updateProfileUser($attributes)) {

                $result = [
                    'message'    => 'Update Profile success.',
                    'statuscode' => 201,
                ];
            } else {

                $result = [
                    'message'    => 'Update Profile failed.',
                    'statuscode' => 422,
                ];
            }
        } else {

            $result = [
                'message'    => 'Password doesn\'t match.',
                'statuscode' => 403,
            ];
        }

        return response()->json($result, $result['statuscode']);
    }
}
