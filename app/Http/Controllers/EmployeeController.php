<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\SaveRequest;
use App\Repository\EmployeeRepositoryInterface;

class EmployeeController extends Controller
{
    protected $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;        
    }

    /**
     * @OA\Get(
     *      path="/api/employee",
     *      operationId="getEmployeeList",
     *      tags={"Employee"},
     *      summary="Get list of employee",
     *      description="Returns list of employee",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(
     *          description="List All employee.",
     *          response=200,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="List All employee."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="200"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Employee")
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Employee Not found.",
     *          response=404,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Employee Not found."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="404"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     *  )
     */

    public function index()
    {
        $employees = $this->employeeRepository->all();

        if (count($employees) > 0) {

            $result = [
                'message'    => 'List All employee.',
                'statuscode' => 200,
                'data'       => $employees
            ];
        } else {

            $result = [
                'message'    => 'Employee Not found.',
                'statuscode' => 404,
                'data'       => []
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

    /**
     * @OA\Post(
     *      path="/api/employee",
     *      operationId="storeEmployee",
     *      tags={"Employee"},
     *      summary="Store new employee",
     *      description="Returns create status",
     *      security={{"bearerAuth":{}}},
     *      @OA\RequestBody(
     *          description="List of user object",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(ref="#/components/schemas/Employee")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/Employee")
     *          )
     *      ),
     *      @OA\Response(
     *          description="Store Employee success.",
     *          response=201,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Store Employee success."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="201"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Store Employee failed.",
     *          response=422,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Store Employee failed."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="422"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     * )
     */

    public function store(SaveRequest $request)
    {
        $attributes = $request->only([
            'nip', 'full_name', 'nick_name', 'birth_date',
            'address', 'phone', 'mobile', 'email'
        ]);

        if ($this->employeeRepository
                ->create($attributes)) {

            $result = [
                'message'    => 'Store Employee success.',
                'statuscode' => 201,
            ];
        } else {

            $result = [
                'message'    => 'Store Employee failed.',
                'statuscode' => 422,
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

    /**
     * @OA\Get(
     *      path="/api/employee/{id}",
     *      operationId="getEmployeeById",
     *      tags={"Employee"},
     *      summary="Get employee information",
     *      description="Returns employee data",
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="Employee id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          description="Employee found.",
     *          response=200,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Employee found."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="200"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  ref="#/components/schemas/Employee"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Employee Not found.",
     *          response=404,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Employee Not found."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="404"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     * )
     */

    public function show($id)
    {
        $employee = $this->employeeRepository->find($id);

        if ($employee) {

            $result = [
                'message'    => 'Employee found.',
                'statuscode' => 200,
                'data'       => $employee
            ];
        } else {

            $result = [
                'message'    => 'Employee Not found.',
                'statuscode' => 404,
                'data'       => $employee
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

    /**
     * @OA\Put(
     *      path="/api/employee/{id}",
     *      operationId="updateEmployee",
     *      tags={"Employee"},
     *      summary="Update existing employee",
     *      description="Returns updated employee status",
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="Employee id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          description="List of user object",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/Employee")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/Employee")
     *          )
     *      ),
     *      @OA\Response(
     *          description="Update Employee success.",
     *          response=201,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Update Employee success."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="201"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Update Employee failed.",
     *          response=422,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Update Employee failed."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="422"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     * )
     */

    public function update(SaveRequest $request, $id)
    {
        $attributes = $request->only([
            'nip', 'full_name', 'nick_name', 'birth_date',
            'address', 'phone', 'mobile', 'email'
        ]);

        $attributes['id'] = $id;

        if ($this->employeeRepository
                ->update($attributes)) {

            $result = [
                'message'    => 'Update Employee success.',
                'statuscode' => 201,
            ];
        } else {

            $result = [
                'message'    => 'Update Employee failed.',
                'statuscode' => 422,
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

    /**
     * @OA\Delete(
     *      path="/api/employee/{id}",
     *      operationId="deleteEmployee",
     *      tags={"Employee"},
     *      summary="Delete existing employee",
     *      description="Deletes a record and returns status",
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(
     *          name="id",
     *          description="Employee id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          description="Employee deleted.",
     *          response=200,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Employee deleted."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="200"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="Employee Not found.",
     *          response=404,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Employee Not found."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="404"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="Unauthorized.",
     *          response=401,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Unauthorized."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="401"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items()
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($id)
    {
        $employee = $this->employeeRepository->delete($id);

        if ($employee) {

            $result = [
                'message'    => 'Employee deleted.',
                'statuscode' => 200
            ];
        } else {

            $result = [
                'message'    => 'Employee Not found.',
                'statuscode' => 404
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

}
