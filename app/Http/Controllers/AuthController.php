<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Libraries\Helpers;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @OA\Post(
     *      path="/api/register",
     *      operationId="registerUser",
     *      tags={"Auth"},
     *      summary="Register new user",
     *      description="Returns register status",
     *      @OA\RequestBody(
     *          description="List of user profile object",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  required={
     *                      "name","address","phone","email","password"
     *                  },
     *                  @OA\Property(
     *                      property="name", type="string",
     *                      title="Name", description="Name of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="address", type="string",
     *                      title="Address", description="Address of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="phone", type="string",
     *                      title="Phone Number", description="Phone Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="mobile", type="string",
     *                      title="Mobile Number", description="Mobile Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="email", type="string",
     *                      title="Email", description="Email of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="Password", description="Password of the new user"
     *                  ),
     *              )
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  required={
     *                      "name","address","phone","email","password"
     *                  },
     *                  @OA\Property(
     *                      property="name", type="string",
     *                      title="Name", description="Name of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="address", type="string",
     *                      title="Address", description="Address of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="phone", type="string",
     *                      title="Phone Number", description="Phone Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="mobile", type="string",
     *                      title="Mobile Number", description="Mobile Number of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="email", type="string",
     *                      title="Email", description="Email of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="Password", description="Password of the new user"
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="User Registration success.",
     *          response=201,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="User Registration success."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="201"
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="User Registration failed.",
     *          response=422,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="User Registration failed."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="422"
     *              )
     *          )
     *      )
     * )
     */

    public function register(RegisterRequest $request)
    {
        $attributes = $request->only([
            'name', 'address', 'phone', 'mobile',
            'email', 'password'
        ]);

        if ($this->userRepository
                ->createUserProfile($attributes)) {

            $result = [
                'message'    => 'User Registration success.',
                'statuscode' => 201,
            ];
        } else {

            $result = [
                'message'    => 'User Registration failed.',
                'statuscode' => 422,
            ];
        }

        return response()->json($result, $result['statuscode']);
    }

    /**
     * @OA\Post(
     *      path="/api/login",
     *      operationId="loginUser",
     *      tags={"Auth"},
     *      summary="Login user",
     *      description="Returns user profile and API Token",
     *      @OA\RequestBody(
     *          description="User credential",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  required={
     *                      "email","password"
     *                  },
     *                  @OA\Property(
     *                      property="email", type="string",
     *                      title="Email", description="Email of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="Password", description="Password of the new user"
     *                  ),
     *              )
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  required={
     *                      "email","password"
     *                  },
     *                  @OA\Property(
     *                      property="email", type="string",
     *                      title="Email", description="Email of the new user",
     *                  ),
     *                  @OA\Property(
     *                      property="password", type="string", format="password", writeOnly=true,
     *                      minimum="8", title="Password", description="Password of the new user"
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          description="User Login success.",
     *          response=200,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="User Login success."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="200"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  @OA\Property(
     *                      property="user",
     *                      @OA\Property(
     *                          property="id", type="integer",
     *                          title="ID", description="ID of the new user",
     *                          example="1"
     *                      ),
     *                      @OA\Property(
     *                          property="email", type="string",
     *                          title="Email", description="Email of the new user",
     *                          example="user@mail.com"
     *                      ),
     *                      @OA\Property(
     *                          property="profile",
     *                          ref="#/components/schemas/Employee"
     *                      ),
     *                  ),
     *                  @OA\Property(
     *                      property="api_token", type="string",
     *                      title="API Token", description="API Token from auth login result",
     *                      example="eyJpdiI6IlZHOXp0WkM2ajZ4VEljMmo4Q1RrMkE9PSIsInZhbHVlIjoiMXgvYnRqeGVyVXVWR1EvaUxoWG82ZzcxRSt1ZGl3TG0wQnZiT1BZMUxYZz0iLCJtYWMiOiJlM2M3YzQ4N2FmZDk5MGNkOTJkZGNkODEyZTg3NzBiZTMyZjZjOWJkMjI0YzM3NzAyMDkwYWUzMDU0Mjc5NjA1In0="
     *                  )
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          description="User Login failed.",
     *          response=422,
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  example="User Login failed."
     *              ),
     *              @OA\Property(
     *                  property="statuscode",
     *                  type="integer",
     *                  example="422"
     *              )
     *          )
     *      )
     * )
     */

    public function login(LoginRequest $request)
    {
        $credential = $request->only(['email', 'password']);

        $user = $this->userRepository->findBy('email', $credential['email']);

        if (Hash::check($credential['password'], @$user->password)) {

            $this->userRepository
                ->update([
                    'id'        => $user->id,
                    'api_token' => Helpers::generateToken()
                ]);

            $user = $this->userRepository->findUserProfie($user->id);

            $result = [
                'message'    => 'User Login success.',
                'statuscode' => 200,
                'data'       => [
                    'user'      => $user,
                    'api_token' => $user->api_token
                ]
            ];
        } else {

            $result = [
                'message'    => 'User Login failed.',
                'statuscode' => 442,
                'data'       => []
            ];
        }

        return response()->json($result, $result['statuscode']);
    }
}
