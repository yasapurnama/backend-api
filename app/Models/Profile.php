<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Profile",
 *     description="Profile model",
 *     required={
 *         "user_id","name","address","phone"
 *     },
 *     @OA\Xml(
 *         name="Profile"
 *     )
 * )
 */

class Profile extends Model
{
    use HasFactory;

    /**
     * @OA\Property(
     *    property="id", type="integer",
     *    title="ID", description="ID of the new profile",
     *    example="1"
     * ),
     * @OA\Property(
     *    property="user_id", type="integer", writeOnly=true,
     *    title="User ID", description="User ID of the new user"
     * ),
     * @OA\Property(
     *    property="name", type="string",
     *    title="Name", description="Name of the new user",
     *    example="Alexis"
     * ),
     * @OA\Property(
     *    property="address", type="string",
     *    title="Address", description="Address of the new user",
     *    example="5691 Zella Locks Apt. 678, Lake Ambrosefurt, WI 43793-4790"
     * ),
     * @OA\Property(
     *    property="phone", type="string",
     *    title="Phone Number", description="Phone Number of the new user",
     *    example="(516) 426-1414"
     * ),
     * @OA\Property(
     *    property="mobile", type="string",
     *    title="Mobile Number", description="Mobile Number of the new user",
     *    example="+1.265.304.1538"
     * ),
     *
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'address',
        'phone', 'mobile'
    ];

    protected $hidden = [
        'user_id', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
