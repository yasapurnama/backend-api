<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Employee",
 *     description="Employee model",
 *     required={
 *         "nip","full_name","nick_name","birth_date",
 *         "address","phone","email"
 *     },
 *     @OA\Xml(
 *         name="Employee"
 *     )
 * )
 */

class Employee extends Model
{
    use HasFactory;

    /**
     * @OA\Property(
     *    property="id", type="integer", readOnly="true",
     *    title="ID", description="ID of the new employee",
     *    example="1"
     * ),
     * @OA\Property(
     *    property="nip", type="string",
     *    title="NIP", description="NIP of the new employee",
     *    example="342376984"
     * ),
     * @OA\Property(
     *    property="full_name", type="string",
     *    title="Full Name", description="Full Name of the new employee",
     *    example="Bert Lueilwitz"
     * ),
     * @OA\Property(
     *    property="nick_name", type="string",
     *    title="Nick Name", description="Nick Name of the new employee",
     *    example="Bert"
     * ),
     * @OA\Property(
     *    property="birth_date", type="string",
     *    title="Birth Date", description="Birth Date of the new employee",
     *    example="1987-11-18"
     * ),
     * @OA\Property(
     *    property="address", type="string",
     *    title="Address", description="Address of the new employee",
     *    example="8257 Zakary Isle Suite 423, South Clotildefurt, CT 27516"
     * ),
     * @OA\Property(
     *    property="phone", type="string",
     *    title="Phone", description="Phone of the new employee",
     *    example="+1.305.813.4139"
     * ),
     * @OA\Property(
     *    property="mobile", type="string",
     *    title="Mobile Number", description="Mobile Number of the new employee",
     *    example="+15598208670"
     * ),
     * @OA\Property(
     *    property="email", type="string",
     *    title="Email", description="Email of the new employee",
     *    example="ondricka.tony@hotmail.com"
     * ),
     * @OA\Property(
     *    property="created_at", type="string", readOnly="true",
     *    title="Timestamps Created", description="Timestamps Created of the new employee",
     *    example="2021-03-11 01:26:50"
     * ),
     * @OA\Property(
     *    property="updated_at", type="string", readOnly="true",
     *    title="Timestamps Updated", description="Timestamps Updated of the new employee",
     *    example="2021-03-11 01:26:50"
     * ),
     *
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip', 'full_name', 'nick_name', 'birth_date',
        'address', 'phone', 'mobile', 'email'
    ];

}
