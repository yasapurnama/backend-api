<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;

/**
 * @OA\Schema(
 *     title="User",
 *     description="User model",
 *     required={
 *         "email","password"
 *     },
 *     @OA\Xml(
 *         name="User"
 *     )
 * )
 */

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * @OA\Property(
     *    property="id", type="integer",
     *    title="ID", description="ID of the new user",
     *    example="1"
     * ),
     * @OA\Property(
     *    property="email", type="string",
     *    title="Email", description="Email of the new user",
     *    example="user@mail.com"
     * ),
     * @OA\Property(
     *    property="password", type="string", format="password", writeOnly=true,
     *    minimum="8", title="Password", description="Password of the new user"
     * ),
     * @OA\Property(
     *    property="api_token", type="string", writeOnly=true,
     *    title="API Token", description="API Token from auth login result"
     * ),
     *
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'api_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token',
        'created_at', 'updated_at'
    ];

    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }
}
