<?php 

namespace App\Libraries;

use Illuminate\Support\Facades\Crypt;

class Helpers {

    public static function generateToken()
    {
        $numbers      = '0123456789';
        $letters      = 'abcdefghijklmnopqrstuvwxyz';
        $strings      = $numbers . $letters . strtoupper($letters);
        $randomString = substr(str_shuffle($strings),1,16);

        return Crypt::encrypt($randomString);
    }
}