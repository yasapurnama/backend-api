<?php

namespace App\Repository;

interface ProfileRepositoryInterface
{
    public function findProfileUser($id);

    public function updateProfileUser(array $attributes);
}
