<?php

namespace App\Repository;

use App\Models\User;

interface UserRepositoryInterface
{
    public function createUserProfile(array $attributes);

    public function findBy($attribute, $value);

    public function findUserProfie($id);
}
