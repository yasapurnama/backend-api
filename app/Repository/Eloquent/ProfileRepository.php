<?php

namespace App\Repository\Eloquent;

use App\Models\Profile;
use App\Repository\ProfileRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProfileRepository extends BaseRepository implements ProfileRepositoryInterface
{
    protected $model;

    public function __construct(Profile $model)
    {
        parent::__construct($model);
    }

    public function findProfileUser($id)
    {
        return $this->model->with('user')
                    ->where('id', $id)
                    ->first();
    }

    public function updateProfileUser(array $attributes)
    {
        $profile = $this->findProfileUser(@$attributes['id']);

        DB::beginTransaction();

        try {
            $profile->user->update([
                'email'    => $attributes['email'],
                'password' => $attributes['new_password'] ??
                                $attributes['password']
            ]);

            $profile->update([
                'name'    => $attributes['name'],
                'address' => $attributes['address'],
                'phone'   => $attributes['phone'],
                'mobile'  => @$attributes['mobile']
            ]);

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            return false;
        }
    }
}