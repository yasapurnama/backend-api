<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\ProfileRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected $model;

    protected $profileRepository;

    public function __construct(User $model, ProfileRepositoryInterface $profileRepository)
    {
        parent::__construct($model);

        $this->profileRepository = $profileRepository;
    }

    public function createUserProfile(array $attributes)
    {
        DB::beginTransaction();

        try {
            $user = $this->create([
                'email'    => $attributes['email'],
                'password' => $attributes['password']
            ]);

            $this->profileRepository->create([
                'user_id' => $user->id,
                'name'    => $attributes['name'],
                'address' => $attributes['address'],
                'phone'   => $attributes['phone'],
                'mobile'  => @$attributes['mobile']
            ]);

            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollback();

            return false;
        }
    }

    public function findBy($attribute, $value)
    {
        return $this->model
                    ->where($attribute, $value)
                    ->first();
    }

    public function findUserProfie($id)
    {
        return $this->model->with('profile')
                    ->where('id', $id)
                    ->first();
    }
}