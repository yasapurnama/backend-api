<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface
{
    public function all(): Collection;

    public function create(array $attributes): Model;

    public function find($id): ?Model;

    public function update(array $attributes);

    public function delete($id);
}
